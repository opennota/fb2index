// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"embed"
	"fmt"
	"io/fs"
	"path"
	"strings"

	"golang.org/x/text/language"
)

type MessageMap map[string]string

type Bundle struct {
	defaultLang language.Tag
	matcher     language.Matcher
	tags        []language.Tag
	messages    map[language.Tag]MessageMap
}

type Localizer struct {
	bundle *Bundle
	lang   language.Tag
}

var i18nBundle *Bundle

func getI18nMessages(fs embed.FS, filename string) (MessageMap, error) {
	f, err := fs.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	m := make(MessageMap)
	s := bufio.NewScanner(f)
	for s.Scan() {
		key, value, ok := strings.Cut(s.Text(), "=")
		if !ok {
			continue
		}
		m[key] = value
	}
	return m, nil
}

//go:embed i18n
var i18n embed.FS

func NewBundle(defaultLang language.Tag) (*Bundle, error) {
	files, _ := fs.Glob(i18n, "i18n/messages.*")
	m := make(map[language.Tag]MessageMap, len(files))
	tags := []language.Tag{
		language.Russian,
	}
	for _, fn := range files {
		lang, err := language.Parse(strings.TrimPrefix(path.Ext(fn), "."))
		if err != nil {
			return nil, fmt.Errorf("failed to find language for file extension %s: %w", path.Ext(fn), err)
		}
		m[lang], err = getI18nMessages(i18n, fn)
		if err != nil {
			return nil, fmt.Errorf("failed to get messages from %s: %w", fn, err)
		}
		if !strings.HasSuffix(fn, ".ru") {
			tags = append(tags, lang)
		}
	}
	return &Bundle{
		defaultLang: defaultLang,
		matcher:     language.NewMatcher(tags),
		tags:        tags,
		messages:    m,
	}, nil
}

func NewLocalizer(bundle *Bundle, langs ...language.Tag) *Localizer {
	_, i, _ := bundle.matcher.Match(langs...)
	return &Localizer{
		bundle: bundle,
		lang:   bundle.tags[i],
	}
}

func (b *Bundle) Matcher() language.Matcher { return b.matcher }

func (b *Bundle) Messages(lang language.Tag) MessageMap {
	m := b.messages[lang]
	if m == nil {
		m = b.messages[b.defaultLang]
	}
	return m
}

func (l *Localizer) Tr(s string) string {
	m := l.bundle.Messages(l.lang)
	if m == nil || m[s] == "" {
		return s
	}
	return m[s]
}

func firstNonEmpty(strs ...string) string {
	for _, s := range strs {
		if s != "" {
			return s
		}
	}
	return ""
}

func (l *Localizer) TrPluralize(s string, n int) string {
	m := l.bundle.Messages(l.lang)
	d := l.bundle.Messages(l.bundle.defaultLang)
	if m == nil {
		m = d
	}
	if n == 1 {
		return firstNonEmpty(m[s], d[s], s)
	}
	key := s + "_plural"
	return firstNonEmpty(m[key], d[key], s)
}
