// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"io"
	"net/http"
	"os/exec"
	"strings"
	"testing"
	"time"
)

func TestBasicFunctionality(t *testing.T) {
	if err := exec.Command("go", "build").Run(); err != nil {
		t.Fatal(err)
	}
	testFrom(t, "testdata/zipped")
	testFrom(t, "testdata/unzipped")
}

func testFrom(t *testing.T, dir string) {
	t.Helper()

	cmd := exec.Command("./fb2index", "-r", dir)
	if err := cmd.Start(); err != nil {
		t.Fatalf("%s: %v\n", dir, err)
	}
	defer cmd.Process.Kill() //nolint:errcheck

	time.Sleep(1 * time.Second)

	// Index page

	resp, err := http.Get("http://localhost:8080/")
	if err != nil {
		t.Fatalf("%s: %v\n", dir, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatalf("%s: want HTTP OK, got %v\n", dir, resp.StatusCode)
	}
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("%s: %v\n", dir, err)
	}
	if !bytes.Contains(data, []byte("</html>")) {
		t.Fatalf("%s: got incomplete HTML\n", dir)
	}
	if !bytes.Contains(data, []byte(`<div class="book">`)) {
		t.Fatalf("%s: want a book in the list, got none\n", dir)
	}

	// Book page

	resp, err = http.Get("http://localhost:8080/b?id=1")
	if err != nil {
		t.Fatalf("%s: %v\n", dir, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatalf("%s: want HTTP OK, got %v\n", dir, resp.StatusCode)
	}
	data, err = io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("%s: %v\n", dir, err)
	}
	if !bytes.Contains(data, []byte("</html>")) {
		t.Fatalf("%s: got incomplete HTML\n", dir)
	}
	if !bytes.Contains(data, []byte(`<div class="annotation">`)) {
		t.Fatalf("%s: want an annotation, got none\n", dir)
	}
	if !bytes.Contains(data, []byte(`Это тестовый файл FictionBook 2.0`)) {
		t.Fatalf("%s: want annotation to contain `Это тестовый файл FictionBook 2.0`\n", dir)
	}

	// Search

	resp, err = http.Post("http://localhost:8080/search", "application/x-www-form-urlencoded", strings.NewReader("query=войны"))
	if err != nil {
		t.Fatalf("%s: %v\n", dir, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatalf("%s: want HTTP OK, got %v\n", dir, resp.StatusCode)
	}
	data, err = io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("%s: %v\n", dir, err)
	}
	if !bytes.Contains(data, []byte("</html>")) {
		t.Fatalf("%s: got incomplete HTML\n", dir)
	}
	if !bytes.Contains(data, []byte(`<div class="book">`)) {
		t.Fatalf("%s: want a search result, got none\n", dir)
	}
	if !bytes.Contains(data, []byte(`<a class="book-link" href="./b?id=1">Война и мир</a>`)) {
		t.Fatalf("%s: want a link to the found book, got none\n", dir)
	}
}
