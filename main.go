// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"golang.org/x/text/language"
)

var (
	dataSource        = flag.String("db", "file::memory:?cache=shared", "SQLite/MySql database. See README.ru.md for details")
	indexFile         = flag.String("idxfile", "", "Index file name to store (if it doesn't exist) or load")
	addr              = flag.String("http", "127.0.0.1:8080", "HTTP service [address][:port]")
	recursive         = flag.Bool("r", false, "Recursively search for .zip files")
	parallel          = flag.Int("j", runtime.NumCPU(), "Number of parallel jobs")
	languages         = flag.String("l", "", "Comma-separated languages (default: all)")
	allowAllLanguages = flag.Bool("a", false, "Accept all language codes, even invalid ones (non-lowercase, more than 2 letters etc.)")

	booksPerPage     = flag.Int("bpp", 50, "Books per page")
	authorsPerPage   = flag.Int("app", 50, "Authors per page")
	sequencesPerPage = flag.Int("spp", 50, "Sequences per page")

	cssPath = flag.String("css", "", "Use CSS file")

	indexerVariant = flag.String("indexer", "internal", "How search works. Possible value: internal, like")

	allowedLanguages []string

	indexed int
)

func isRegular(fi os.FileInfo) bool {
	return fi.Mode()&os.ModeType == 0
}

func walker(index func(string)) filepath.WalkFunc {
	return func(path string, fi os.FileInfo, _ error) error {
		if !isRegular(fi) {
			return nil
		}

		if !strings.HasSuffix(path, ".zip") && !strings.HasSuffix(path, ".fb2") {
			return nil
		}

		index(path)

		return nil
	}
}

func main() {
	flag.Usage = func() {
		w := flag.CommandLine.Output()
		fmt.Fprintf(w, "Usage: fb2index [options] [file1 file2 ... fileN]\n")
		flag.PrintDefaults()
	}
	log.SetFlags(0)
	flag.Parse()
	if *languages != "" {
		allowedLanguages = strings.Split(*languages, ",")
	}

	switch *indexerVariant {
	case "like":
		indexerType = indexerLike
	case "fulltext":
		indexerType = indexerFulltext
	default:
		indexerType = indexerInternal
	}

	var err error
	if i18nBundle, err = NewBundle(language.Russian); err != nil {
		log.Fatalln("failed to load i18n bundle:", err)
	}

	initDB()
	jobs := make(chan book, *parallel)
	indexer := NewIndexer(jobs)

	index := func(name string) {
		start := time.Now()
		var err error
		if strings.HasSuffix(name, ".zip") {
			err = indexZIP(name, jobs)
		}
		if strings.HasSuffix(name, ".fb2") {
			err = indexFB2(name, jobs)
		}
		if err != nil {
			log.Printf("%s: open: %v", name, err)
		} else {
			log.Printf("Indexed %s in %v\n", name, time.Since(start))
			indexed++
		}
	}

	start := time.Now()

	for _, path := range flag.Args() {
		if *recursive {
			fi, err := os.Stat(path)
			if err != nil {
				log.Printf("%s: stat: %v", path, err)
				continue
			}

			if fi.IsDir() {
				if err := filepath.Walk(path, walker(index)); err != nil {
					log.Printf("%s: %v", path, err)
				}
				continue
			}
		}

		if !strings.HasSuffix(path, ".zip") && !strings.HasSuffix(path, ".fb2") {
			log.Printf("%s: not an indexable file", path)
			continue
		}

		index(path)
	}

	close(jobs)
	<-indexer.Done()
	_ = indexer.Close()

	log.Printf("Indexed %d file(s) in %v", indexed, time.Since(start))
	if indexerType == indexerInternal && *indexFile != "" && indexed > 0 {
		if err := saveTrigrams(); err != nil {
			log.Print(err)
		}
	}
	log.Printf("Server listening on %s", *addr)
	listenAndServe()
}
